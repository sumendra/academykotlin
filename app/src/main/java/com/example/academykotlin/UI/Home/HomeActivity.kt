package com.example.academykotlin.UI.Home

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.academykotlin.R

class HomeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
