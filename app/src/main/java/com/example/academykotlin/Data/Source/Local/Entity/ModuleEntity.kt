package com.example.academykotlin.Data.Source.Local.Entity

public data class ModuleEntity(
    private var mModuleId: String,
    private var mCourseId: String,
    private var mTitle: String,
    private var mPosition: Int,
    private var mRead: Boolean? = false
){
    lateinit var  contentEntity: ContentEntity
}