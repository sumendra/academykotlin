package com.example.academykotlin.Data.Source.Local.Entity

public data class CourseEntity(
    private var courseId: String,
    private var title: String,
    private var description: String,
    private var deadline: String,
    private var book: Boolean?,
    private var imagePath: String
)